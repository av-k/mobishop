(function (angular) {

  'use strict';

  angular
    .module('application.shop.formEditProfile', [])
    .directive('shopFormEditProfileDrv', shopFormEditProfileDrv);

  function shopFormEditProfileDrv() {
    return {
      replace: true,
      restrict: 'E',
      scope: {},
      templateUrl: 'application/shop/directives/form-edit-profile/formEditProfile.html',
      controllerAs: 'formEditProfileCtrl',
      controller: 'FormEditProfileCtrl'
    }
  }

})(angular);
