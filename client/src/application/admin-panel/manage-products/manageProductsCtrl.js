(function(angular) {
  'use strict';

  angular
    .module('application.adminPanel.manageProductsCtrl', [])
    .controller('ManageProductsCtrl', ManageProductsCtrl);


  function ManageProductsCtrl(config, NgTableParams, serverHost, $uibModal, Product, Manufacturer, Carrier) {
    var vm = this,
        filterBy = {
          name: undefined
        };

    // init essence
    vm.config = config;
    vm.apiURL = serverHost + config.restApiRoot;
    vm.orderBy = {createdAt: 'desc'};
    vm.filter = {};

    // Prepare Products
    vm.totalCount = function (params) {
      var reqParams = {};

      if (params && Object.keys(params).length) {
        reqParams.where = params;
      }

      Product.count(reqParams, function(info) {
        vm.tableParams.total(info.count);
      });
    };

    //
    vm.totalCount();
    vm.tableParams = new NgTableParams(
      { page: 1, count: 10 },
      {
        total: 0,
        counts: [10, 25, 50],
        getData: function ($defer, params) {
          var sorting = params.sorting() && Object.keys(params.sorting()).length ? params.sorting() : vm.orderBy,
              count = params.count(),
              page = params.page(),

              reqParams = {
                include: 'getCategory',
                where: filterBy,
                limit: count,
                skip: (page - 1 || 0) * count
              };

          if (sorting && Object.keys(sorting).length) {
            reqParams.order = Object.keys(sorting)[0] + ' ' + sorting[Object.keys(sorting)[0]].toUpperCase();
          }

          Product.find({filter: reqParams}, function(products) {
            $defer.resolve(products);
          });
        }
      }
    );

    //
    Manufacturer.find({}, function (manufacturers) {
      if (manufacturers && manufacturers.length) {
        var arr = [{name: 'All'}];
        arr = arr.concat(manufacturers);
        vm.manufacturers = arr;
      }
    });

    //
    Carrier.find({}, function (carriers) {
      if (carriers && carriers.length) {
        var arr = [{name: 'All'}];
        arr = arr.concat(carriers);
        vm.carriers = arr;
      }
    });

    //
    vm.setFilter = function (by, value) {
      if (by === 'search') {
        filterBy['name'] = { regexp: '/' + value + '/i' };
      }
      if (by === 'inStock') {
        if (value) {
          filterBy['quantity'] = { gt: 0 };
        } else {
          delete filterBy['quantity'];
        }
      }
      if (by === 'manufacturer') {
        if (value) {
          filterBy['manufacturerId'] = value;
        } else {
          delete filterBy['manufacturerId'];
        }
      }
      if (by === 'carrier') {
        if (value) {
          filterBy['carrierId'] = value;
        } else {
          delete filterBy['carrierId'];
        }
      }
      if (by === 'status') {
        if (value) {
          filterBy[by] = 1;
        } else {
          delete filterBy[by];
        }
      }

      vm.totalCount(filterBy);
      vm.tableParams.reload();
    };

    // DELETE
    vm.delete = function (product) {
      $uibModal.open({
        templateUrl: 'application/admin-panel/modal-dialog/confirmation/confirmation.html',
        bindToController: true,
        controllerAs: 'modalConfirmationCtrl',
        controller: function ($uibModalInstance) {
          var vmDelete = this;

          vmDelete.title = 'Confirm the action';
          vmDelete.text = 'Are you sure you want to remove a product id: `' + product.id + '`?';

          vmDelete.ok = function () {
            Product.destroyById({id: product.id}, function () {
              vm.tableParams.reload();
              $uibModalInstance.close();
            });
          };

          vmDelete.cancel = function () {
            $uibModalInstance.dismiss('cancel');
          };
        }
      });
    };

    // EDITE
    vm.edit = function (product) {
      $uibModal.open({
        templateUrl: 'application/admin-panel/modal-dialog/add-product/addProduct.html',
        controllerAs: 'modalAddProductCtrl',
        controller: 'ModalEditProductCtrl',
        resolve: {
          params: function() {
            return {
              product: product,
              tableParams: vm.tableParams
            }
          }
        }
      });
    };

    // MODAL: CREATE PRODUCT
    vm.addProduct = function () {
      $uibModal.open({
        templateUrl: 'application/admin-panel/modal-dialog/add-product/addProduct.html',
        controllerAs: 'modalAddProductCtrl',
        controller: 'ModalAddProductCtrl',
        resolve: {
          tableParams: function() {
            return vm.tableParams
          }
        }
      });
    }
  }


})(angular);
