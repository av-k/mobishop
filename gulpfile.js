gulp = require('gulp');
dir = require('require-dir');

var serverDev = 'http://localhost:3000';
var serverProd = 'http://altava.net:3000';

var knownOptions = {
        default: {
            env: 'dev',
            config: 'client/src',
            server: serverDev
        }
    },
    minimist = require('minimist');

options = minimist(process.argv.slice(2), knownOptions);
config = require('./' + options.config + '/config.json');

if (options['env'] === 'production') {
    config.isProduction = true;
    config.server = serverProd;
    console.info('Production build')
}

console.info('ENV: ', options['env']);
console.info('SERVER: ', options['server']);

modules = {};

onErrors = function (error) {
    console.info(error.toString());
};

dir('./gulp');

gulp.task('default', ['build'], function () {
  gulp.start('watch');
  gulp.start('webServer');
});
