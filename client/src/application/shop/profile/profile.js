(function (angular) {
  'use strict';

  angular
    .module('application.shop.profile', [])
    .config(config);


  function config($stateProvider) {

    $stateProvider
      .state('application.shop.profile', {
        url: '/profile',
        templateUrl: 'application/shop/profile/profile.html',
        controllerAs: 'profileCtrl',
        controller: profileCtrl
      });
  }

  function profileCtrl() {
    var vm = this;

  }

})(angular);
