(function (angular) {
  'use strict';

  angular
    .module('application.adminPanel', [])
    .config(config);

  function config($stateProvider) {

    $stateProvider
      .state('application.adminPanel', {
        url: 'admin-panel',
        templateUrl: 'application/admin-panel/index.html',
        controller: AdminPanelCtrl,
        controllerAs: 'adminPanelCtrl'
      });
  }

  function AdminPanelCtrl($state, $rootScope) {
    var vm = this;

    //
    vm.$rootScope = $rootScope;

    $state.go('application.adminPanel.manageProducts');
  }

})(angular);
