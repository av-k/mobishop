(function(angular) {
  'use strict';

  angular
    .module('application.shop.productsCtrl', [])
    .controller('ProductsCtrl', ProductsCtrl);


  function ProductsCtrl($rootScope, $state, config, serverHost, NgTableParams, Category, Product) {
    var vm = this,
        filterBy = {
          name: undefined
        };

    // init essence
    vm.config = config;
    vm.$rootScope = $rootScope;
    vm.apiURL = serverHost + config.restApiRoot;
    vm.currentCategory = $state.params.category.toLowerCase();
    vm.orderBy = {createdAt: 'desc'};
    vm.filter = {};

    if (!$state.params.category) {
      return;
    }

    // prepare current category
    var patternCategory = '/' + $state.params.category + '/i';
    Category.findOne({filter: {where: {name: {regexp: patternCategory}}}}, function (category) {
      initProductsList(category.id)
    });

    // preload products
    function initProductsList(categoryId) {
      tableTotalCount({filter: {where: {categoryId: categoryId}}});
      initTable({categoryId: categoryId});
    }

    // init tables content
    function initTable(where) {
      vm.tableParams = new NgTableParams(
        { page: 1, count: 10 },
        {
          total: 0,
          counts: [10, 25, 50],
          getData: function ($defer, params) {
            var sorting = params.sorting() && Object.keys(params.sorting()).length ? params.sorting() : vm.orderBy,
              count = params.count(),
              page = params.page(),

              reqParams = {
                where: where || filterBy,
                limit: count,
                skip: (page - 1 || 0) * count
              };

            if (sorting && Object.keys(sorting).length) {
              reqParams.order = Object.keys(sorting)[0] + ' ' + sorting[Object.keys(sorting)[0]].toUpperCase();
            }

            Product.find({filter: reqParams}, function(products) {
              $defer.resolve(products);
            });
          }
        }
      );
    }

    // prepare total counter of products
    function tableTotalCount(params, where) {
      var reqParams = where || {};

      if (!reqParams) {
        if (params && Object.keys(params).length) {
          reqParams.where = params;
        }
      }

      Product.count(reqParams, function(info) {
        vm.tableParams.total(info.count);
      });
    }

    // echo product info
    vm.echoInfo = function (product) {
      var availableInfoFields = ['description', 'soft', 'hardware'];
      console.log(product)
    };


  }


})(angular);
