(function(angular) {
  'use strict';

  angular
    .module('application.shop.productCtrl', [])
    .controller('ProductCtrl', ProductCtrl);


  function ProductCtrl($rootScope, $state, config, serverHost, Product) {
    var vm = this;

    // INIT ESSENCE
    vm.config = config;
    vm.category = $state.params.category;
    vm.$rootScope = $rootScope;
    vm.apiURL = serverHost + config.restApiRoot;
    vm.product = {};

    // GET PRODUCT INFO
    Product.findById({id: $state.params.id}, function (product) {
      prepareProductData(product, $state.params.category);
    });

    // PREPARE ECHO DATA
    function prepareProductData(product, category) {
      if (category === 'phones') {
        for (var pName in product) {
          if (pName.indexOf('$') === -1 && pName.indexOf('Id') === -1 && pName !== 'toJSON') {
            vm.product[pName] = product[pName];
          }
        }
      }
    }
  }


})(angular);
