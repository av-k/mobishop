(function(angular) {

  'use strict';

  angular
    .module('application.adminPanel.formAddProductPhoneDrv', [])
    .directive('formAddProductPhoneDrv', formAddProductPhoneDrv);

  function formAddProductPhoneDrv() {
    return {
      replace: true,
      restrict: 'E',
      scope: {
        product: '=',
        temp: '='
      },
      bindToController: true,
      templateUrl: 'application/admin-panel/directives/form-add-product-phone/formAddProductPhone.html',
      controller: FormAddProductPhoneCtrl,
      controllerAs: 'formAddProductPhoneCtrl'
    }
  }

  function FormAddProductPhoneCtrl($helper, config) {
    var vm = this;

    // init essence
    vm.config = config;
    vm.validate = $helper.validate;

    // remove image
    vm.removeImage = function (image, event) {
      //$ngfBlobUrl
      if (vm.product.images && vm.product.images.length) {
        vm.product.images = vm.product.images.filter(function (path) {
          var imageParts = image.$ngfBlobUrl.split('/'),
              imageName = imageParts[imageParts.length - 1];

          if (path.indexOf(imageName) === -1) {
            return path;
          }
        });
      }

      vm.temp.images = vm.temp.images.filter(function (item) {
        if (item.$$hashKey !== image.$$hashKey) {
          return item;
        }
      });
    };

    // upload images
    vm.uploadImages = function($files, $invalidFiles) {
      if (!vm.temp.errors.images) {
        vm.temp.errors.images = [];
      }

      if ($invalidFiles && $invalidFiles.length) {
        $invalidFiles.forEach(function (item) {
          if (item.$error === 'pattern') {
            vm.temp.errors.images.push('File format is not supported (only: ' + config.validation.product.image.types.join(",") + ')');
          }
          if (item.$error === 'minWidth') {
            vm.temp.errors.images.push('Image height is less than required (' + config.validation.product.image.minWidth + 'px)');
          }
          if (item.$error === 'minHeight') {
            vm.temp.errors.images.push('Image height is less than required (' + config.validation.product.image.minHeight + 'px)');
          }
          if (item.$error === 'maxSize') {
            vm.temp.errors.images.push('Image size is exceeded (max: ' + config.validation.product.image.maxSize + ')');
          }
        });
      } else if ($files && $files.length) {
        vm.temp.images = $files;
      }
    };

  }

})(angular);
