(function (angular) {
  'use strict';

  angular
    .module('application.adminPanel.manageOrders', [])
    .config(config);


  function config($stateProvider) {

    $stateProvider
      .state('application.adminPanel.manageOrders', {
        url: '/manage-orders',
        templateUrl: 'application/admin-panel/manage-orders/manageOrders.html',
        controllerAs: 'manageOrdersCtrl',
        controller: 'ManageOrdersCtrl'
      });
  }

})(angular);
