(function (angular) {

  'use strict';

  angular
    .module('application.formResetPassword', [])
    .directive('formResetPassword', formResetPassword);

  function formResetPassword() {
    return {
      replace: true,
      restrict: 'E',
      scope: {
        token: '='
      },
      bindToController: true,
      templateUrl: 'directives/form-reset-password/formResetPassword.html',
      controllerAs: 'formResetPassword',
      controller: FormResetPassword
    }
  }

  function FormResetPassword(config, $validationRules, Notification, $timeout, $state, UserModel) {
    var vm = this;

    // init essence
    vm.config = config;
    vm.validators = $validationRules;

    // on change fields
    vm.changeFields = function() {
      vm.hasSubmit = false;
      vm.error = null;
      vm.success = null;
    };

    // on submit
    vm.submit = function() {
      if (vm.password !== vm.rePassword) {
        vm.error = 'Passwords are different';
      } else if (!vm.token) {
        vm.error = 'Token field is invalid';
      } else {
        vm.error = null;
        requestSetPassword(vm.token, vm.password);
      }

      vm.error && Notification.error(vm.error);

      vm.hasSubmit = true;
      vm.password = null;
      vm.rePassword = null;
    };

    // set new password by token
    var requestSetPassword = function(token, password) {
      UserModel._setPasswordByToken({token: token, password: password},
        function(user) {
          Notification.success('Password has been change');

          $timeout(function () {
            $state.go('application.signIn');
          }, 6000);
        }, function(err) {
          if (err.data && err.data.error && err.data.error.message) {
            vm.error = err.data.error.message;
          } else {
            vm.error = 'Process of change password was failed';
          }

          Notification.error(vm.error);
        }
      );
    }
  }

})(angular);
