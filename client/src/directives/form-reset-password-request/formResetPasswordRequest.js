(function (angular) {

  'use strict';

  angular
    .module('application.formResetPasswordRequest', [])
    .directive('formResetPasswordRequest', formResetPasswordRequest);

  function formResetPasswordRequest() {
    return {
      replace: true,
      restrict: 'E',
      scope: {
        backurl: '='
      },
      templateUrl: 'directives/form-reset-password-request/formResetPasswordRequest.html',
      controllerAs: 'formResetPasswordRequest',
      controller: FormResetPasswordRequest
    }
  }

  function FormResetPasswordRequest($scope, config, $validationRules, Notification, UserModel) {
    var vm = this;

    // init essence
    vm.config = config;
    vm.validators = $validationRules;
    vm.backUrl = $scope.backurl;

    // on change field
    vm.changeFields = function() {
      vm.hasSubmit = false;
      vm.error = null;
      vm.success = null;
    };

    // on submit
    vm.submit = function() {
      UserModel.resetPassword({email: vm.email},
        function(status) {
          Notification.success('Check your email <strong>' + vm.email + '</strong>');

          vm.hasSubmit = true;
          vm.email = null;
        },
        function(err) {
          if (err.data && err.data.error && err.data.error.message) {
            vm.error = err.data.error.message;
          } else {
            vm.error = 'Process request password was failed';
          }
          Notification.error(vm.error);

          vm.hasSubmit = true;
          vm.email = null;
        }
      );
    };
  }

})(angular);
