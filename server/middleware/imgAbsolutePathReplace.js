var app = require('../server');

module.exports = function() {

  return function imgAbsolutePathReplace(req, res, next) {

    var send = res.send;
    res.send = function (data) {
      var body = data instanceof Buffer ? data.toString() : data;

      if (typeof body === 'string') {
        var protocol = !req.secure ? 'http' : 'https',
            baseUrl = req.headers.host,
            imagesFolders = app.get('imagesFolders'),
            url = protocol + '://' + baseUrl + app.get('tempImages'),
            patternImage = '"([a-z\-_0-9\/\:\.]*\.(jpe?g|png|gif)")',
            folders,
            patternFullPath;

        if (imagesFolders && imagesFolders.length) {
          folders = imagesFolders.map(function (folder) { return '"/' + folder; }).join('|');
          patternFullPath = new RegExp(folders + patternImage, 'g');

          body = body.replace(patternFullPath, function (source) {
            return '"' + url + source.substring(1, source.length);
          });
        }
      }

      send.call(this, body);
    };

    next();
  }
};
