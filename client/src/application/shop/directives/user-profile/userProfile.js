(function(angular) {

  'use strict';

  angular
    .module('application.shop.userProfile', [])
    .directive('userProfileDrv', userProfileDrv);

  function userProfileDrv() {
    return {
      replace: true,
      restrict: 'E',
      scope: {},
      templateUrl: 'application/shop/directives/user-profile/userProfile.html',
      controller: UserProfileCtrl,
      controllerAs: 'userProfileCtrl'
    }
  }

  function UserProfileCtrl($rootScope, $state, $uibModal, $user) {
    var vm = this;

    // init essence
    vm.$rootScope = $rootScope;

    // sign in
    vm.signIn = function () {
      $uibModal.open({
        templateUrl: 'application/shop/modal-dialog/sign-in/signIn.html',
        size: 'sm',
        controllerAs: 'modalSignInCtrl',
        controller: function ($uibModalInstance) {
          var vmSI = this;

          // init essence
          vmSI.title = 'Login';
          vmSI.$uibModalInstance = $uibModalInstance;
        }
      });
    };

    // sign up
    vm.signUp = function () {
      $uibModal.open({
        templateUrl: 'application/shop/modal-dialog/sign-up/signUp.html',
        size: 'sm',
        controllerAs: 'modalSignUpCtrl',
        controller: function ($uibModalInstance) {
          var vmSI = this;

          // init essence
          vmSI.title = 'Registration';
          vmSI.$uibModalInstance = $uibModalInstance;
        }
      });
    };

    //
    vm.logout = function () {
      $user.logout();
      $state.go('application.shop');
    };
  }

})(angular);
