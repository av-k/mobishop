(function (angular) {

  'use strict';

  angular.module('application.notificationSettings', ['ui-notification'])
    .config(function(NotificationProvider) {
      NotificationProvider.setOptions({
        positionX: 'right',
        positionY: 'bottom',
        delay: 5000,
        startTop: 10,
        startRight: 10,
        verticalSpacing: 16,
        horizontalSpacing: 16
      });
    });

})(angular);
