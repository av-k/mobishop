(function (angular) {
  'use strict';

  angular
    .module('application.shop.signIn', [])
    .config(config);

  function config($stateProvider) {

    $stateProvider
      .state('application.shop.signIn', {
        templateUrl: 'application/shop/modal-dialog/sign-in/signIn.html'
      });
  }

})(angular);
