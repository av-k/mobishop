(function (angular) {

  'use strict';

  angular
    .module('application.formResetPasswordRequest', [])
    .directive('formResetPasswordRequest', formResetPasswordRequest);

  function formResetPasswordRequest() {
    return {
      replace: true,
      restrict: 'E',
      scope: {},
      templateUrl: 'directives/form-reset-password-request/formResetPasswordRequest.html',
      controllerAs: 'formResetPasswordRequest',
      controller: FormResetPasswordRequestCtrl
    }
  }

  function FormResetPasswordRequestCtrl(config, $validationRules, UserModel) {
    var vm = this;

    vm.config = config;
    vm.validators = $validationRules;

    //
    vm.changeFields = function() {
      vm.hasSubmit = false;
      vm.error = null;
      vm.success = null;
    };

    //
    vm.submit = function() {
      UserModel._resetPasswordRequest({email: vm.email},
        function(status) {
          vm.success = 'Check your email'
        },
        function(err) {
          if (err.data && err.data.error && err.data.error.message) {
            vm.error = err.data.error.message;
          } else {
            vm.error = 'Request password was failed';
          }
        }
      );

      vm.hasSubmit = true;
      vm.email = null;
    };
  }

})(angular);
