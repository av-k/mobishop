(function (angular) {
  'use strict';

  angular
    .module('application.adminPanel.editProfile', [])
    .config(config);


  function config($stateProvider) {

    $stateProvider
      .state('application.adminPanel.editProfile', {
        url: '/edit-profile',
        templateUrl: 'application/admin-panel/edit-profile/editProfile.html',
        controllerAs: 'editProfileCtrl',
        controller: 'EditProfileCtrl'
      });
  }

})(angular);
