(function(angular) {
  'use strict';

  angular
    .module('application.adminPanel.modalAddProductCtrl', [])
    .controller('ModalAddProductCtrl', AddBrandCtrl);


  function AddBrandCtrl($uibModalInstance, $validationRules, $createProduct, Category, Manufacturer, Carrier, tableParams) {
    var vm = this;

    // init essence
    vm.modalTitle = 'Add a new product';
    vm.validators = $validationRules;
    vm.temp = {
      uploadProgress: {},
      errors: {
        uploadError: {}
      }
    };
    vm.product = {
      images: []
    };
    vm.currentCategory = {};
    vm.currentManufacturer = {};
    vm.currentCarrier = {};

    // init categories
    Category.find({}, function (categories) {
      vm.temp.categories = categories;
    });

    // init manufacturers
    Manufacturer.find({}, function (manufacturers) {
      vm.temp.manufacturers = manufacturers;
    });

    // init carriers
    Carrier.find({}, function (carriers) {
      vm.temp.carriers = carriers;
    });

    // set category
    vm.setCategory = function (category) {
      vm.currentCategory.name = category.name;
      vm.product.categoryId = category.id;
    };

    // set manufacturer
    vm.setManufacturer = function (manufacturer) {
      vm.currentManufacturer.name = manufacturer.name;
      vm.product.manufacturerId = manufacturer.id;
    };

    // set carrier
    vm.setCarrier = function (carrier) {
      vm.currentCarrier.name = carrier.name;
      vm.product.carrierId = carrier.id;
    };

    // init essence params
    function initEssenceParams(productType) {
      if (productType === 'Phones') {
        vm.product = {
          soft: {},
          hardware: {},
          battery: {},
          connectivity: {},
          display: {},
          sizeAndWeight: {
            dimensions: {}
          }
        };
      }
    }

    //
    vm.submit = function () {
      var productParams = {
        temp: vm.temp,
        tableParams: tableParams,
        $uibModalInstance: $uibModalInstance
      };

      $createProduct(vm.product, productParams);
    };

    //
    vm.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  }

})(angular);
