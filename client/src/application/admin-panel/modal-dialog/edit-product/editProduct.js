(function(angular) {
  'use strict';

  angular
    .module('application.adminPanel.modalEditProductCtrl', [])
    .controller('ModalEditProductCtrl', ModalEditProductCtrl);


  function ModalEditProductCtrl($uibModalInstance, $validationRules, $editProduct, serverHost, config, Category, Manufacturer, Carrier, params) {
    var vm = this,
        product = params.product;

    // init essence
    vm.modalTitle = 'Edit product';
    vm.validators = $validationRules;
    vm.product = product;
    vm.temp = {
      uploadProgress: {},
      errors: {
        uploadError: {}
      }
    };

    // set current: category
    Category.findById({id: product.categoryId}, function (category) {
      vm.currentCategory = category;
    });

    // set current: manufacturer
    Manufacturer.findById({id: product.manufacturerId}, function (manufacturer) {
      vm.currentManufacturer = manufacturer;
    });

    // set current: carrier
    Carrier.findById({id: product.carrierId}, function (carrier) {
      vm.currentCarrier = carrier;
    });

    // set current: images
    if (params.product.images && params.product.images.length) {
      vm.temp.images = params.product.images.map(function (item) {
        return { $ngfBlobUrl: serverHost + config.restApiRoot + item };
      });
    }

    // init categories
    Category.find({}, function (categories) {
      vm.temp.categories = categories;
    });

    // init manufacturers
    Manufacturer.find({}, function (manufacturers) {
      vm.temp.manufacturers = manufacturers;
    });

    // init carriers
    Carrier.find({}, function (carriers) {
      vm.temp.carriers = carriers;
    });

    // set category
    vm.setCategory = function (category) {
      vm.currentCategory.name = category.name;
      vm.product.categoryId = category.id;
    };

    // set manufacturer
    vm.setManufacturer = function (manufacturer) {
      vm.currentManufacturer.name = manufacturer.name;
      vm.product.manufacturerId = manufacturer.id;
    };

    // set carrier
    vm.setCarrier = function (carrier) {
      vm.currentCarrier.name = carrier.name;
      vm.product.carrierId = carrier.id;
    };

    //
    vm.submit = function () {
      var productParams = {
        temp: vm.temp,
        tableParams: params.tableParams,
        $uibModalInstance: $uibModalInstance
      };

      $editProduct(vm.product, productParams);
    };

    //
    vm.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  }

})(angular);
