(function (angular) {

  'use strict';

  angular
    .module('application.adminPanel.formEdit', [])
    .directive('adminFormEditDrv', adminFormEditDrv);

  function adminFormEditDrv() {
    return {
      replace: true,
      restrict: 'E',
      scope: {},
      templateUrl: 'application/admin-panel/directives/form-edit/formEdit.html',
      controllerAs: 'formEditCtrl',
      controller: FormEditCtrl
    }
  }

  function FormEditCtrl($rootScope, $validationRules, Notification, config, UserModel) {
    var vm = this;

    vm.user = {
      email: $rootScope.userInfo ? $rootScope.userInfo.email : null,
      username: $rootScope.userInfo ? $rootScope.userInfo.username : null
    };
    vm.config = config;
    vm.validators = $validationRules;

    //
    vm.changeFields = function() {
      delete vm.errorAuth;
    };

    //
    vm.submit = function() {
      if (vm.user.password !== vm.user.repassword) {
        return Notification.error({message: 'Passwords do not match', delay: 3000});
      }

      var updateData = {};

      for (var pName in vm.user) {
        if (vm.user[pName]) {
          if (pName === 'oldpassword') {
            updateData['oldPassword'] = vm.user[pName];
          }

          if (pName === 'email' || pName === 'username' || pName === 'password') {
            updateData[pName] = vm.user[pName];
          }
        }
      }

      UserModel.prototype$updateAttributes({id: $rootScope.userInfo.id}, updateData,
        function (user) {
          delete vm.user.password;
          delete vm.user.oldpassword;
          delete vm.user.repassword;

          Notification.success({message: 'User credentials have been updated', delay: 3000});
        },
        function (err) {
          var msg = 'Process of change user credentials was failed';

          delete vm.user.password;
          delete vm.user.oldpassword;
          delete vm.user.repassword;

          if (err && err.data && err.data.error && err.data.error.message) {
            msg = err.data.error.message;
          }

          Notification.error(msg);
        }
      );
    }
  }

})(angular);
