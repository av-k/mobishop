(function (angular) {
  'use strict';

  angular
    .module('application.shop.signUp', [])
    .config(config);

  function config($stateProvider) {

    $stateProvider
      .state('application.shop.signUp', {
        templateUrl: 'application/shop/modal-dialog/sign-up/signUp.html'
      });
  }

})(angular);
