var config = require('../config');

module.exports = function(server) {

  server.dataSources.storage.connector.allowedContentTypes = config.storageFormats;
  server.dataSources.storage.connector.maxFileSize = config.storageMaxSize * 1024 * 1024;

  server.dataSources.storage.connector.getFilename = function(file, req, res) {
    var nowTime = new Date().getTime(),
        nowTimePart = String(nowTime).slice(7),
        rNum = Math.random() + Number(nowTimePart),
        nameArr = file.name.split('.'),
        format = nameArr[nameArr.length - 1];

    return Math.floor(rNum * 100000).toString(36) + nowTimePart + '.' + format;
  }

};
