var app = require('./../server');

module.exports = function(server) {

  app.use(app.loopback.context());
  app.use(app.loopback.token({ model: app.models.AccessTokenModel }));

  app.use(function (req, res, next) {
    var loopbackContext = app.loopback.getCurrentContext();

    if (loopbackContext) loopbackContext.set('headers', req.headers);

    if (!req.accessToken) return next();
    app.models.UserModel.findOne({include: 'getRoles', where: {id: req.accessToken.userId}}, function(err, user) {
      if (err) return next(err);
      if (!user) return next(new Error('User not found.'));
      user.roles = user.toJSON().getRoles.map(function (role) { return role.name; });
      res.locals.currentUser = user;
      if (loopbackContext) loopbackContext.set('currentUser', user);
      next();
    });
  });

};
