var Promise = require('promise'),
    manufacturers = require('./manufacturers'),
    carriers = require('./carriers'),
    categories = require('./categories'),
    phones = require('./phones');

module.exports = {
  up: function (dataSource, next) {

    Promise.all([
      dataSource.models.Manufacturer.create(manufacturers, function (err, manufacturers) {
        if (!err) {
          Promise.resolve();
        } else {
          console.log('Migration: creating "Manufacturers" was failed');
          Promise.reject(err);
        }
      }),
      dataSource.models.Carrier.create(carriers, function (err, carriers) {
        if (!err) {
          Promise.resolve();
        } else {
          console.log('Migration: creating "Carrier" was failed');
          Promise.reject(err);
        }
      }),
      dataSource.models.Category.create(categories, function (err, categories) {
        if (!err) {
          Promise.resolve();
        } else {
          console.log('Migration: creating "Categories" was failed');
          Promise.reject(err);
        }
      }),
      dataSource.models.Product.create(phones, function (err, products) {
        if (!err) {
          Promise.resolve();
        } else {
          console.log('Migration: creating "Products" was failed');
          Promise.reject(err);
        }
      })

    ]).then(function (response) {
      console.log('Migration: process was finished!');
      next();
    }).catch(function(err) {
      console.log('Migration: process was broken');
      next();
    });

  },
  down: function (dataSource, next) {

    Promise.all([
      dataSource.models.Manufacturer.deleteAll({}, function (err, info) {
        if (!err) {
          Promise.resolve();
        } else {
          console.log('Migration: removing "Manufacturers" was failed');
          Promise.reject(err);
        }
      }),
      dataSource.models.Carrier.deleteAll({}, function (err, info) {
        if (!err) {
          Promise.resolve();
        } else {
          console.log('Migration: removing "Carrier" was failed');
          Promise.reject(err);
        }
      }),
      dataSource.models.Category.deleteAll({}, function (err, info) {
        if (!err) {
          Promise.resolve();
        } else {
          console.log('Migration: removing "Categories" was failed');
          Promise.reject(err);
        }
      }),
      dataSource.models.Product.deleteAll({}, function (err, info) {
        if (!err) {
          Promise.resolve();
        } else {
          console.log('Migration: removing "Products" was failed');
          Promise.reject(err);
        }
      })

    ]).then(function (response) {
      console.log('Migration: remove state is OK');
      next();
    }).catch(function(err) {
      console.log('Migration: remove state is broken');
      next();
    });

  }
};
