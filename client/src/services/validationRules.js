(function (angular) {

  'use strict';

  angular
    .module('$validationRules', [])
    .service('$validationRules', validationRules);

  function validationRules(config) {
    var validators = {};

    //
    validators.email = [
      {
        message: '`email` is totally required.',
        rule: function (form, field) {
          if (field) {
            return (field.$dirty || form.$submitted) && field.$error.required;
          }
        }
      },
      {
        message: '`email` should match myuser@domain.name pattern',
        rule: function (form, field) {
          if (field) {
            if (!field) {
              return false;
            }
            return field.$viewValue && field.$error.pattern;
          }
        }
      }
    ];

    //
    validators.password = [
      {
        message: '`password` is required',
        rule: function (form, field) {
          if (field) {
            return (field.$dirty || form.$submitted) && field.$error.required;
          }
        }
      },
      {
        message: '`password` must be longer than ' + config.validation.user.password.min + ' characters.',
        rule: function (form, field) {
          if (field) {
            return field.$error.minlength;
          }
        }
      }
    ];

    //
    validators.username = [
      {
        message: '`username` is totally required.',
        rule: function (form, field) {
          if (field) {
            return (field.$dirty || form.$submitted) && field.$error.required;
          }
        }
      },
      {
        message: 'use letters and numbers (a-Z0-9) only please',
        rule: function (form, field) {
          if (field) {
            return field.$error.pattern;
          }
        }
      },
      {
        message: '`username` must be longer than ' + config.validation.user.username.min + ' characters.',
        rule: function (form, field) {
          if (field) {
            return field.$error.minlength;
          }
        }
      },
      {
        message: '`username` must be less than ' + config.validation.user.username.max + ' characters.',
        rule: function (form, field) {
          if (field) {
            return field.$error.maxlength;
          }
        }
      }
    ];

    //
    validators.productName = [
      {
        message: '`name` is totally required.',
        rule: function (form, field) {
          if (field) {
            return (field.$dirty || form.$submitted) && field.$error.required;
          }
        }
      },
      {
        message: '`name` must be longer than ' + config.validation.product.name.min + ' characters.',
        rule: function (form, field) {
          if (field) {
            return field.$error.minlength;
          }
        }
      },
      {
        message: '`name` must be less than ' + config.validation.product.name.max + ' characters.',
        rule: function (form, field) {
          if (field) {
            return field.$error.maxlength;
          }
        }
      }
    ];

    //
    validators.productDescription = [
      {
        message: '`description` is totally required.',
        rule: function (form, field) {
          if (field) {
            return (field.$dirty || form.$submitted) && field.$error.required;
          }
        }
      },
      {
        message: '`description` must be longer than ' + config.validation.product.description.min + ' characters.',
        rule: function (form, field) {
          if (field) {
            return field.$error.minlength;
          }
        }
      },
      {
        message: '`description` must be less than ' + config.validation.product.description.max + ' characters.',
        rule: function (form, field) {
          if (field) {
            return field.$error.maxlength;
          }
        }
      }
    ];

    //
    validators.productBatteryType = [
      {
        message: '`type` must be less than ' + config.validation.product.battery.type.max + ' characters.',
        rule: function (form, field) {
          if (field) {
            return field.$error.maxlength;
          }
        }
      }
    ];

    validators.connectivityCell = [
      {
        message: '`type` must be less than ' + config.validation.product.connectivity.cell.max + ' characters.',
        rule: function (form, field) {
          if (field) {
            return field.$error.maxlength;
          }
        }
      }
    ];

    return validators;
  }

})(angular);
