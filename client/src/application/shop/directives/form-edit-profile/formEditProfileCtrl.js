(function(angular) {
  'use strict';

  angular
    .module('application.shop.formEditProfileCtrl', [])
    .controller('FormEditProfileCtrl', FormEditProfileCtrl);


  function FormEditProfileCtrl($rootScope, $validationRules, Notification, config, $userInfo, UserModel) {
    var vm = this;

    vm.user = {
      email: null,
      username: null,
      phone: null
    };
    vm.config = config;
    vm.validators = $validationRules;

    // INIT USER INFO
    $rootScope.$on('getUserInfo', function (event, userInfo) {
      if (userInfo) {
        vm.user.email = userInfo.email;
        vm.user.username = userInfo.username;
        vm.user.phone = userInfo.phone;
      }
    });

    //
    vm.changeFields = function() {
      delete vm.errorAuth;
    };

    //
    vm.submit = function() {
      if (vm.user.password !== vm.user.repassword) {
        return Notification.error({message: 'Passwords do not match', delay: 3000});
      }

      var updateData = {};

      for (var pName in vm.user) {
        if (vm.user[pName]) {
          if (pName === 'oldpassword') {
            updateData['oldPassword'] = vm.user[pName];
          }

          if (pName === 'email' || pName === 'username' || pName === 'password' || pName === 'phone') {
            updateData[pName] = vm.user[pName];
          }
        }
      }

      UserModel.prototype$updateAttributes({id: $rootScope.userInfo.id}, updateData,
        function (user) {
          delete vm.user.password;
          delete vm.user.oldpassword;
          delete vm.user.repassword;

          Notification.success({message: 'User credentials have been updated', delay: 3000});
        },
        function (err) {
          var msg = 'Process of change user credentials was failed';

          delete vm.user.password;
          delete vm.user.oldpassword;
          delete vm.user.repassword;

          if (err && err.data && err.data.error && err.data.error.message) {
            msg = err.data.error.message;
          }

          Notification.error(msg);
        }
      );
    }
  }


})(angular);
