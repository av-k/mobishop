(function (angular) {

  'use strict';

  angular
    .module('$helper', [])
    .service('$helper', helper);

  function helper() {
    var helper = {};

    helper.validate = function (value, type) {
      if (type === 'number') {
        if (/^[0-9]+$/.test(value)) {
          return parseInt(value);
        } else {
          return null;
        }
      }
    };

    return helper;
  }

})(angular);
