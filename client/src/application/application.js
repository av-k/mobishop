(function (angular) {
  'use strict';

  angular
    .module('application', [
      'ui.router',
      'ngResource',
      'lbServices',
      'ui.bootstrap',
      'ngAnimate',
      'angularMoment',
      'angularUtils.directives.dirPagination',
      'toggle-switch',
      'ngFileUpload',
      'mgr.validation',
      'ui-notification',
      'ngTable',
      '$helper',
      '$user',
      '$userInfo',
      '$createProduct',
      '$editProduct',
      '$validationRules',
      'application.config',
      'application.error-404',
      'application.error-500',
      'application.resetPasswordRequest',
      'application.formResetPasswordRequest',
      'application.resetPassword',
      'application.formResetPassword',
      'application.adminPanel',
      'application.adminPanel.signIn',
      'application.adminPanel.formSignIn',
      'application.adminPanel.adminHeader',
      'application.adminPanel.manageOrders',
      'application.adminPanel.manageOrdersCtrl',
      'application.adminPanel.manageProducts',
      'application.adminPanel.manageProductsCtrl',
      'application.adminPanel.modalAddProductCtrl',
      'application.adminPanel.formAddProductPhoneDrv',
      'application.adminPanel.modalEditProductCtrl',
      'application.adminPanel.editProfile',
      'application.adminPanel.editProfileCtrl',
      'application.adminPanel.formEdit',
      'application.shop',
      'application.shop.signIn',
      'application.shop.signUp',
      'application.shop.formSignIn',
      'application.shop.formSignUp',
      'application.shop.shopHeader',
      'application.shop.searchPanel',
      'application.shop.shopFooter',
      'application.shop.userProfile',
      'application.shop.buy',
      'application.shop.products',
      'application.shop.productsCtrl',
      'application.shop.product',
      'application.shop.productCtrl',
      'application.shop.profile',
      'application.shop.formEditProfile',
      'application.shop.formEditProfileCtrl',
      'application.notificationSettings'
    ])
    .config(config)
    .constant('serverHost', '{{SERVER_HOST}}')
    .run(run);

  config.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', 'LoopBackResourceProvider', '$httpProvider',  'config', 'serverHost'];
  function config($stateProvider, $urlRouterProvider, $locationProvider, LoopBackResourceProvider, $httpProvider, config, serverHost) {
    LoopBackResourceProvider.setAuthHeader('X-Access-Token');
    LoopBackResourceProvider.setUrlBase(serverHost + config.restApiRoot);

    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    });

    $httpProvider.interceptors.push(function ($rootScope, $q, LoopBackAuth, $location) {
      return {
        responseError: function (rejection) {
          if (rejection.status == 401) {
            $rootScope.$broadcast('status-401', rejection);
            delete $rootScope.userInfo;
            LoopBackAuth.clearUser();
            LoopBackAuth.clearStorage();
          } else if (rejection.status == 404) {
            if (rejection.data.error.code === 'MODEL_NOT_FOUND') {
              LoopBackAuth.clearUser();
              LoopBackAuth.clearStorage();
              $location.nextAfterLogin = $location.path();
            }
          } else if (rejection.status == 422) {
            $rootScope.$broadcast('status-422', rejection);
          } else if (rejection.status == -1) {
            $location.path('/error-500', rejection);
          }
          return $q.reject(rejection);
        }
      };
    });

    $stateProvider
      .state('application', {
        url: '/',
        templateUrl: 'application/application.html'
      });
    $urlRouterProvider.otherwise('shop');
  }

  function run($rootScope, LoopBackAuth, $state, $userInfo) {
    $rootScope.$on('$stateChangeSuccess', function(event, toState){
      $rootScope.stateActive = toState.name;

      // get user data
      $userInfo.getUserInfo()
        .then(function(userInfo) {
          $rootScope.userInfo = userInfo;
          $rootScope.$broadcast('getUserInfo', userInfo);

          // CHECK STATE ACCESS (ADMIN-PANEL)
          if (toState.templateUrl.indexOf('admin-panel') !== -1 || $state.current.accessRoles) {
            if (toState.name.indexOf('resetPassword') !== -1 || toState.name.indexOf('resetPasswordRequest') !== -1) {
              return;
            }

            if (!userInfo || userInfo && userInfo.roles.indexOf('administrator') === -1) {
              delete $rootScope.userInfo;
              LoopBackAuth.clearUser();
              LoopBackAuth.clearStorage();
              $state.go('application.adminPanel.signIn');
            }
          }
        });

      // CHECK STATE ACCESS (SHOP)
      if (toState.url === '/') {
        $state.go('application.shop');
      }

    });
  }

})(angular);
