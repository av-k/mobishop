(function (angular) {
  'use strict';

  angular
    .module('application.shop.product', [])
    .config(config);


  function config($stateProvider) {

    $stateProvider
      .state('application.shop.product', {
        url: '/product/:id?category',
        templateUrl: 'application/shop/product/product.html',
        controllerAs: 'productCtrl',
        controller: 'ProductCtrl'
      });
  }

})(angular);
