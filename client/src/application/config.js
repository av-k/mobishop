(function() {

  'use strict';

  var config = {
    restApiRoot: '/api/v1',
    currency: '$',
    validation: {
      user: {
        username: {
          pattern: /^[a-zA-Z0-9]+$/,
          min: 3,
          max: 35
        },
        email: {
          pattern: /^\w+@[a-zA-Z_0-9-]+?\.[a-zA-Z]{2,5}$/
        },
        password: {
          min: 6
        },
        firstName: {
          max: 36
        },
        lastName: {
          max: 36
        },
        phone: {
          pattern: /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/i,
          min: 7
        }
      },
      product: {
        name: {
          min: 3,
          max: 35
        },
        description: {
          min: 10,
          max: 400
        },
        features: {
          min: 10,
          max: 200
        },
        battery: {
          type: {
            max: 100
          }
        },
        connectivity: {
          cell: {
            max: 100
          }
        },
        image: {
          types: ["jpg", "png"],
          maxSize: "5MB",
          minWidth: 200,
          minHeight: 200
        }
      }
    },
    managePages: {
      itemsPerPage: 5
    }
  };

  /* --------------------------------- */

  angular
    .module('application.config', [])
    .constant("config", config);

})(angular);
