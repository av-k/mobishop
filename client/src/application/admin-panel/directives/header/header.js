(function(angular) {

  'use strict';

  angular
    .module('application.adminPanel.adminHeader', [])
    .directive('adminHeaderDrv', adminHeaderDrv);

  function adminHeaderDrv() {
    return {
      replace: true,
      restrict: 'E',
      scope: {},
      templateUrl: 'application/admin-panel/directives/header/header.html',
      controller: AdminHeaderCtrl,
      controllerAs: 'adminHeaderCtrl'
    }
  }

  function AdminHeaderCtrl($user) {
    var vm = this;

    //
    vm.logout = function () {
      $user.logout();
    };
  }

})(angular);
