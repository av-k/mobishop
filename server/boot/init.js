/*
**
** ANY SCRIPTS INITIALIZATION
**
*/


var defaultRoles = require('./../../common/boot/defaultRoles');
var defaultUsers = require('./../../common/boot/defaultUsers');

module.exports = function (server) {

  //process.env.DEFAULT_DATA = true;
  if (process.env.DEFAULT_DATA) {
    defaultRoles(server);
    defaultUsers(server);
  }

};
