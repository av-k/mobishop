(function (angular) {
  'use strict';

  angular
    .module('application.adminPanel.signIn', [])
    .config(config);

  function config($stateProvider) {

    $stateProvider
      .state('application.adminPanel.signIn', {
        url: '/sign-in',
        templateUrl: 'application/admin-panel/sign-in/signIn.html'
      });
  }

})(angular);
