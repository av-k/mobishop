(function(angular) {

  'use strict';

  angular
    .module('application.shop.shopHeader', [])
    .directive('shopHeaderDrv', shopHeaderDrv);

  function shopHeaderDrv() {
    return {
      replace: true,
      restrict: 'E',
      scope: {},
      templateUrl: 'application/shop/directives/header/header.html',
      controller: ShopHeaderCtrl,
      controllerAs: 'shopHeaderCtrl'
    }
  }

  function ShopHeaderCtrl($rootScope, Category) {
    var vm = this;

    vm.$rootScope = $rootScope;

    //
    Category.find({}, function (categories) {
      vm.categories = categories;
    });

    //
    vm.logout = function () {
      //$user.logout();
      //$state.go('application.signIn');
    };
  }

})(angular);
