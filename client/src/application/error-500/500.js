(function (angular) {
  'use strict';

  angular
    .module('application.error-500', [])
    .config(config);

  function config($stateProvider) {

    $stateProvider
      .state('application.error-500', {
        url: "500",
        templateUrl: "application/error-500/505.html"
      });
  }

})(angular);
