(function(angular) {
  'use strict';

  angular
    .module('application.adminPanel.manageOrdersCtrl', [])
    .controller('ManageOrdersCtrl', ManageOrdersCtrl);


  function ManageOrdersCtrl(config, NgTableParams, Order) {
    var vm = this;

    // init essence
    vm.orderBy = {createdAt: 'desc'};

    // Prepare Products
    vm.totalCount = function (params) {
      var reqParams = {};

      if (params && Object.keys(params).length) {
        reqParams.where = params;
      }

      Order.count(reqParams, function(info) {
        vm.tableParams.total(info.count);
      });
    };


    //
    vm.totalCount();
    vm.tableParams = new NgTableParams(
      { page: 1, count: 10 },
      {
        total: 0,
        counts: [10, 25, 50],
        getData: function ($defer, params) {
          var sorting = params.sorting() && Object.keys(params.sorting()).length ? params.sorting() : vm.orderBy,
              count = params.count(),
              page = params.page(),

              reqParams = {
                where: {},
                include: [
                  {
                    relation: 'getUser',
                    scope: {
                      fields: ['id', 'email', 'phone']
                    }
                  },
                  {
                    relation: 'getProduct',
                    scope: {
                      fields: ['id', 'name']
                    }
                  }
                ],
                limit: count,
                skip: (page - 1 || 0) * count
              };

          if (sorting && Object.keys(sorting).length) {
            reqParams.order = Object.keys(sorting)[0] + ' ' + sorting[Object.keys(sorting)[0]].toUpperCase();
          }

          Order.find({filter: reqParams}, function(orders) {
            $defer.resolve(orders);
          });
        }
      }
    );
  }


})(angular);
