(function (angular) {

  'use strict';

  angular
    .module('$user', [])
    .service('$user', user);

  function user($state, $q, LoopBackAuth, UserModel, $userInfo) {

    this.login = function (credentials) {
      var deferred = $q.defer();

      UserModel.login(credentials,
        function (res) {
          $userInfo.setUserInfo(res.user);
          deferred.resolve(res);
        }, function (res) {
          deferred.reject(res.data);
        });

      return deferred.promise;
    };

    this.logout = function () {
      $userInfo.clearUserInfo();
      LoopBackAuth.clearUser();
      LoopBackAuth.clearStorage();
      $state.go($state.current, {}, {reload: true});
    };

    this.isLogin = function () {
      return UserModel.isAuthenticated();
    };
  }

})(angular);
