(function(angular) {

  'use strict';

  angular
    .module('application.shop.shopFooter', [])
    .directive('shopFooterDrv', shopFooterDrv);

  function shopFooterDrv() {
    return {
      replace: true,
      restrict: 'E',
      scope: {},
      templateUrl: 'application/shop/directives/footer/footer.html',
      controller: ShopFooterCtrl,
      controllerAs: 'shopFooterCtrl'
    }
  }

  function ShopFooterCtrl() {
    var vm = this;

  }

})(angular);
