(function (angular) {
  'use strict';

  angular
    .module('application.error-404', [])
    .config(config);

  function config($stateProvider) {

    $stateProvider
      .state('application.error-404', {
        url: "404",
        templateUrl: "application/error-404/404.html"
      });
  }

})(angular);
