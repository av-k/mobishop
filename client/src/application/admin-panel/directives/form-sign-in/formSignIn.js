(function (angular) {

  'use strict';

  angular
    .module('application.adminPanel.formSignIn', [])
    .directive('adminFormSignInDrv', adminFormSignInDrv);

  function adminFormSignInDrv() {
    return {
      replace: true,
      restrict: 'E',
      scope: {},
      templateUrl: 'application/admin-panel/directives/form-sign-in/formSignIn.html',
      controllerAs: 'formSignInCtrl',
      controller: FormSignInCtrl
    }
  }

  function FormSignInCtrl($state, $rootScope, $user, $userInfo, $validationRules, $timeout, Notification, config) {
    var vm = this;

    vm.user = {};
    vm.config = config;
    vm.validators = $validationRules;

    //
    vm.changeFields = function() {
      delete vm.errorAuth;
    };

    //
    vm.submit = function(form) {
      $user.login(vm.user)
        .then(function(response) {
          vm.errorAuth = null;
          $userInfo.getUserInfo()
            .then(function(userInfo) {
              if (userInfo.roles && userInfo.roles.indexOf('administrator') !== -1) {
                $rootScope.userInfo = userInfo;
                $state.go('application.adminPanel.manageProducts');
              } else {
                $user.logout();
                Notification.error({message: 'You are not an administrator', delay: 3000});

                $timeout(function () {
                  $state.go('application.signIn');
                }, 3500);
              }
            });
        })
        .catch(function(err) {
          Notification.error('Incorrect username or password');
        });
    }
  }

})(angular);
