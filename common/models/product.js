module.exports = function(Product) {

  /*
   ** PREVENTS
   */
  Product.observe('before save', function (ctx, next) {
    var instance = ctx[ctx.instance ? 'instance' : 'currentInstance'];

    if (instance) {
      !instance.createdAt && (instance.createdAt = new Date().getTime());
      instance.modifiedAt = new Date().getTime();
    }

    next();
  });

  //
  Product.afterRemote('create', function (ctx, result, next) {

    Product.findOne({include: 'getCategory', where: {id: ctx.result.id}}, function (err, product) {
      if (!product) {
        return next();
      }

      ctx.result.getCategory = product.getCategory;
      next();
    });

  });

  //
  Product.observe('persist', function(ctx, next) {
    var instance = ctx.currentInstance;

    next();
  });

};
