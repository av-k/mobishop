(function(angular) {

  'use strict';

  angular
    .module('application.shop.searchPanel', [])
    .directive('searchPanelDrv', searchPanelDrv);

  function searchPanelDrv() {
    return {
      replace: true,
      restrict: 'E',
      scope: {},
      templateUrl: 'application/shop/directives/search-panel/searchPanel.html',
      controller: SearchPanelCtrl,
      controllerAs: 'searchPanelCtrl'
    }
  }

  function SearchPanelCtrl(Product) {
    var vm = this;

    //
    vm.getSearch = function (query) {
      if (!query) {
        delete vm.result;
        return;
      }

      Product.find({filter: {include: 'getCategory', where: {name: {regexp: '/' + query + '/i'}}}}, function (products) {
        vm.result = products;
      });
    };

    //
    vm.select = function () {
      delete vm.result;
    };
  }

})(angular);
