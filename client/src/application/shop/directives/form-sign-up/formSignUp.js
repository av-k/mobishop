(function (angular) {

  'use strict';

  angular
    .module('application.shop.formSignUp', [])
    .directive('shopFormSignUpDrv', shopFormSignUpDrv);

  function shopFormSignUpDrv() {
    return {
      replace: true,
      restrict: 'E',
      scope: {
        modalinstance: '='
      },
      bindToController: true,
      templateUrl: 'application/shop/directives/form-sign-up/formSignUp.html',
      controllerAs: 'formSignUpCtrl',
      controller: FormSignUpCtrl
    }
  }

  function FormSignUpCtrl(/*$rootScope, $user, $userInfo, */$validationRules, Notification, config, UserModel) {
    var vm = this;

    vm.user = {};
    vm.config = config;
    vm.validators = $validationRules;

    //
    vm.changeFields = function() {
      delete vm.errorAuth;
    };

    //
    vm.submit = function() {
      if (vm.user.password !== vm.user.repassword) {
        return Notification.error({message: 'Passwords not equal', delay: 3000});
      }

      UserModel.create(vm.user,
        function (response) {
          Notification.success({message: 'User has been created, now you can login', delay: 3000});
          vm.modalinstance.close();
        },
        function (err) {
          if (err.data && err.data.error && err.data.error.details && err.data.error.details.messages) {
            var errors = [];

            for (var pName in err.data.error.details.messages) {
              errors = errors.concat(err.data.error.details.messages[pName]);
            }

            Notification.error({message: errors.join('. '), delay: 3000});
          } else {
            Notification.error({message: 'User creation was failed', delay: 3000});
          }
        }
      )
    }
  }

})(angular);
