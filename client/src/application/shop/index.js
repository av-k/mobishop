(function (angular) {
  'use strict';

  angular
    .module('application.shop', [])
    .config(config);

  function config($stateProvider) {

    $stateProvider
      .state('application.shop', {
        url: 'shop',
        templateUrl: 'application/shop/index.html',
        controller: ShopCtrl,
        controllerAs: 'shopCtrl'
      });
  }

  function ShopCtrl($rootScope) {
    var vm = this;

    vm.$rootScope = $rootScope;

  }

})(angular);
