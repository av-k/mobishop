(function (angular) {

  'use strict';

  angular
    .module('$createProduct', [])
    .service('$createProduct', createProduct);

  function createProduct(config, Upload, serverHost, Product) {
    var run = function(newProduct, params) {

      if (params.temp.images && params.temp.images.length) {
        newProduct.useValidation = true;
      }

      // create product
      function create() {
        Product.create(newProduct,
          function (instance) {
            params.tableParams.data.push(instance);
            params.tableParams.reload();
            params.$uibModalInstance.close();
          },
          function (err) {
            if (err.data && err.data.error && err.data.error.message === 'ok') {
              Upload.upload({
                url: serverHost + config.restApiRoot + '/containers/phones/upload',
                file: params.temp.images
              }).progress(function(evt) {
                params.temp.uploadProgress.pt = parseInt(100.0 * evt.loaded / evt.total);
              }).success(function(reply, status, headers, config) {
                delete newProduct.useValidation;

                if (reply.result.files && Object.keys(reply.result.files).length) {
                  for (var pName in reply.result.files) {
                    var image = reply.result.files[pName][0];
                    newProduct.images.push('/containers/' + image.container + '/download/' + image.name);
                  }
                }

                create();
              }).error(function (reply, status, headers) {
                params.temp.errors.uploadError.msg = reply.error.message || 'Unexpected error: file upload is failed';
              });
            } else {
              params.temp.errors.push('Unexpected error: Creating `product` a failed');
            }
          }
        );
      }
      create();

    };

    return run;
  }

})(angular);
