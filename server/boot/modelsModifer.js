module.exports = function modelCustomization(server) {

  var ObjectID = server.models.UserModel.getDataSource().connector.getDefaultIdType();

  for (var modelName in server.models) {
    var model = server.models[modelName];

    !model.settings.acls && (model.settings.acls = []);
    model.settings.acls.push(
      { principalType: 'ROLE', permission: 'DENY',  principalId: '$everyone',      property: '*' },
      { principalType: 'ROLE', permission: 'DENY',  principalId: '$owner',         property: 'deleteById' },
      { principalType: 'ROLE', permission: 'ALLOW', principalId: 'administrator',  property: '*' },
      { principalType: 'ROLE', permission: 'ALLOW', principalId: '$owner',         property: '*' },
      { principalType: 'ROLE', permission: 'ALLOW', principalId: 'administrator',  property: 'deleteById' }
    );

    // MODEL: RoleMapping
    if (modelName === 'RoleMapping') {
      model.defineProperty('principalId', { type: ObjectID });
    }
    // MODEL: Product
    if (modelName === 'Product') {
      model.defineProperty('manufacturerId', { type: ObjectID });
      model.defineProperty('carrierId', { type: ObjectID });
      model.defineProperty('categoryId', { type: ObjectID });
    }
  }
};
