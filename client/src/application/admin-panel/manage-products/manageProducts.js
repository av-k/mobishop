(function (angular) {
  'use strict';

  angular
    .module('application.adminPanel.manageProducts', [])
    .config(config);


  function config($stateProvider) {

    $stateProvider
      .state('application.adminPanel.manageProducts', {
        url: '/manage-products',
        templateUrl: 'application/admin-panel/manage-products/manageProducts.html',
        controllerAs: 'manageProductsCtrl',
        controller: 'ManageProductsCtrl'
      });
  }

})(angular);
