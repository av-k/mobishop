(function (angular) {
  'use strict';

  angular
    .module('application.shop.products', [])
    .config(config);


  function config($stateProvider) {

    $stateProvider
      .state('application.shop.products', {
        url: '/products?category',
        templateUrl: 'application/shop/products/products.html',
        controllerAs: 'productsCtrl',
        controller: 'ProductsCtrl'
      });
  }

})(angular);
