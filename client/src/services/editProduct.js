(function (angular) {

  'use strict';

  angular
    .module('$editProduct', [])
    .service('$editProduct', editProduct);

  function editProduct(config, Upload, serverHost, Product) {
    var run = function(product, params) {

      // save product
      function save() {
        product.$save(function (instance) {

          Upload.upload({
            url: serverHost + config.restApiRoot + '/containers/phones/upload',
            file: params.temp.images
          }).progress(function(evt) {
            params.temp.uploadProgress.pt = parseInt(100.0 * evt.loaded / evt.total);
          }).success(function(reply, status, headers, config) {
            if (reply.result.files && Object.keys(reply.result.files).length) {
              for (var pName in reply.result.files) {
                var image = reply.result.files[pName][0];
                product.images.push('/containers/' + image.container + '/download/' + image.name);
              }
            }

            Product.prototype$updateAttributes({id: product.id}, {images: product.images}, function () {
              params.tableParams.reload();
              params.$uibModalInstance.close();
            });

          }).error(function (reply, status, headers) {
            params.temp.errors.uploadError.msg = reply.error.message || 'Unexpected error: file upload is failed';
          });
          }
        );
      }
      save();


    };

    return run;
  }

})(angular);
