(function(angular) {

  'use strict';

  angular
    .module('application.shop.buy', [])
    .directive('buyDrv', buyDrv);

  function buyDrv() {
    return {
      replace: true,
      restrict: 'E',
      scope: {
        product: '='
      },
      templateUrl: 'application/shop/directives/buy/buy.html',
      controller: ShopBuyCtrl,
      controllerAs: 'shopBuyCtrl'
    }
  }

  function ShopBuyCtrl($rootScope, $scope, Notification, Order) {
    var vm = this;

    //
    vm.buy = function () {
      if ($rootScope.userInfo && $scope.product) {
        var newOrder = {
              productId: $scope.product.id,
              buyerId: $rootScope.userInfo.id,
              amount: 1
            };

        console.log(newOrder)

        Order.create(newOrder, function (info) {
          console.log(info);
          Notification.success({message: 'Your order number issued', delay: 3000})
        });
      }
    };
  }

})(angular);
