var package = require('./../../package'),
    config = require('../../server/config');

module.exports = function (User) {

  /*
   ** ON CREATE
   */
  User.observe('before save', function (ctx, next) {
    var instance = ctx[ctx.instance ? 'instance' : 'currentInstance'];

    if (instance) {
      !instance.createdAt && (instance.createdAt = new Date().getTime());
      instance.modifiedAt = new Date().getTime();
    }

    next();
  });

  //
  User.afterRemote('create' , function(ctx, user, next) {
    var Role = User.app.models.Role,
        RoleMapping = User.app.models.RoleMapping,
        context = User.app.loopback.getCurrentContext(),
        body = ctx.req.body,
        roleName = 'user';

    if (body.isAdmin && context.get('currentUser') && context.get('currentUser').roles.indexOf('administrator') !== -1) {
      roleName = 'administator';
      delete body.isAdmin;
    }

    Role.findOne({where: {name: roleName}}, function (err, role) {
      if (err || !role) {
        return next();
      }

      role.principals.create({
        principalType: RoleMapping.USER,
        principalId: user.id
      }, function (err, principal) {
        if (err) {
          next(err, null);
        } else {
          next();
        }
      });
    });
  });


  /*
   ** LOGIN
   */
  User.afterRemote('login', function (ctx, result, next) {

    User.findOne({include: 'getRoles', where: {id: ctx.result.userId}}, function (err, user) {
      if (user) {
        var resultJSON = ctx.result.toJSON(),
            userJSON = user.toJSON();

        resultJSON.user = {
          email: userJSON.email,
          username: userJSON.username,
          roles: userJSON.getRoles.map(function (role) { return role.name; }) || []
        };

        ctx.result = resultJSON;

        next();
      }
    });
  });


  /*
   ** UPDATE
   */
  User.beforeRemote('prototype.updateAttributes', function (ctx, result, next) {
    var instance = ctx[ctx.instance ? 'instance' : 'currentInstance'],
        context = User.app.loopback.getCurrentContext(),
        currentUser = context.get('currentUser');

    instance.modifiedAt = new Date().getTime();

    // CHANGE USER PASSWORD HOOK
    if (ctx.req.body.password) {
      changePassword(ctx, next, currentUser);
    } else {
      next();
    }
  });

  //
  var changePassword = function (ctx, next) {
    var userId = ctx.req.params.id || ctx.req.accessToken.userId;

    User.findById(userId, function (err, user) {
      if (err || !user) {
        return next('User not found', null);
      }

      var pattern = new RegExp(config.validations.password.pattern, 'g'),
          oldPassword,
          newPassword;

      if (!ctx.req.body.oldPassword) {
        return next('Field `oldPassword` is empty', null);
      }
      if (!pattern.test(ctx.req.body.password)) {
        return next('Incorrect symbols for field `password`', null);
      }

      oldPassword = String(ctx.req.body.oldPassword);
      newPassword = String(ctx.req.body.password);
      delete ctx.req.body.oldPassword;

      if (newPassword.length < config.validations.password.min) {
        return next('Password is too short', null);
      }

      user.hasPassword(oldPassword, function (err, compareResult) {
        if (err || !compareResult) {
          return next('Incorrect password', null);
        }

        return next();
      });
    });
  };



  /*
   ** METHODS
   */
  // SET NEW PASSWORD BY TOKEN
  User._setPasswordByToken = function(token, password, cb) {
    var AccessTokenModel = User.app.models.AccessTokenModel;

    AccessTokenModel.findById(token, function(err, token) {
      if (err || !token) {
        cb(err ? err.message : 'Invalid token', null);
        return;
      }

      token.user(function(err, user) {
        if (err || !user) {
          cb(err ? err.message : 'User not found', null);
          return;
        }

        user.password = password;
        user.save(function(err, instance) {
          if (err) {
            cb(err ? err.message : 'Password change is failed', null);
          } else {
            token.destroy();
            cb(null, {
              id: user.id,
              email: user.email,
              username: user.username
            });
          }
        });
      });
    });
  };

  User.remoteMethod(
    '_setPasswordByToken',
    {
      http: {path: '/reset-password/set'},
      accepts: [
        {arg: 'token', type: 'string', required: true},
        {arg: 'password', type: 'string', required: true}
      ],
      returns: {arg: 'user', type: 'object'}
    }
  );


  /*
   ** INSTANCE EVENTS
   */
  User.on('resetPasswordRequest', emailSendResetPasswordRequest);


  /*
   ** MAIL DELIVERY
   */
  function emailSendResetPasswordRequest(info) {
    var clientUrl = User.app.get('clientUrl')[process.env.NODE_ENV || 'dev'],
        html = 'Hello ' + (info.user.username || '') + '! <br>For reset your password use <a href="' + clientUrl + '/reset-password?access_token=' + info.accessToken.id + '">this link</a>.';

    User.app.models.Email.send({
      to: info.email,
      subject: package.description + ': Password reset',
      html: html
    }, function(err) {
      if (err) return console.log('>! error sending (EMAIL): PASSWORD RESET (' + err + ')');
      console.log('> sending (EMAIL): PASSWORD RESET to:', info.email);
    });
  }
};
