(function (angular) {

  'use strict';

  angular
    .module('application.shop.formSignIn', [])
    .directive('shopFormSignInDrv', shopFormSignInDrv);

  function shopFormSignInDrv() {
    return {
      replace: true,
      restrict: 'E',
      scope: {
        modalinstance: '='
      },
      bindToController: true,
      templateUrl: 'application/shop/directives/form-sign-in/formSignIn.html',
      controllerAs: 'formSignInCtrl',
      controller: FormSignInCtrl
    }
  }

  function FormSignInCtrl($rootScope, $user, $userInfo, $validationRules, Notification, config) {
    var vm = this;

    vm.user = {};
    vm.config = config;
    vm.validators = $validationRules;

    //
    vm.changeFields = function() {
      delete vm.errorAuth;
    };

    //
    vm.submit = function() {
      $user.login(vm.user)
        .then(function(response) {
          vm.errorAuth = null;
          $userInfo.getUserInfo()
            .then(function(userInfo) {
              $rootScope.userInfo = userInfo;
              vm.modalinstance.close();
            });
        })
        .catch(function(err) {
          Notification.error('Incorrect username or password');
        });
    }
  }

})(angular);
